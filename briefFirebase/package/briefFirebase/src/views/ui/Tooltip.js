/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import  secureLocalStorage  from  "react-secure-storage";
import { Tooltip, Button } from 'reactstrap';
import {getDoc, doc} from "firebase/firestore";
import { db } from '../../components/firebase/Firebase';

import BreadCrumbs from '../../layouts/breadcrumbs/BreadCrumbs';
import ComponentCard from '../../components/ComponentCard';

// Utilisation du stockage de session local
secureLocalStorage.setItem('VarToBeSaved', 'mavariable');
const recupvar = secureLocalStorage.getItem("VarToBeSaved");      
console.log(recupvar);


const TooltipItem = (props) => {

/************************** Utilisation de la bdd firebase (firestore) **************/
console.log("test");
const [variableGlobale, setvariableGlobale] = useState("");
const myasyncfunc = async (param1) => {
console.log(param1);
const donnees = await getDoc(doc(db, "/bdd_path"));
setvariableGlobale(donnees);
}
myasyncfunc("parametre1");
console.log("variableGlobale", variableGlobale);
/*************************************************************************************/


  const { item, id } = props;
  const [tooltipOpen, setTooltipOpen] = useState(false);

  const toggle = () => setTooltipOpen(!tooltipOpen);

  return (
    <span>
      <Button className="me-1" color="outline-primary" id={`Tooltip-${id}`}>
        {item.text}
      </Button>
      <Tooltip
        placement={item.placement}
        isOpen={tooltipOpen}
        target={`Tooltip-${id}`}
        toggle={toggle}
      >
        Tooltip Content!
      </Tooltip>
      <p>L&apos;utilisateur connecté est : {secureLocalStorage.getItem('CurrentUser')}</p>
      <p> Données issues de la BDD : {variableGlobale}</p>
    </span>
  );
};

const TooltipComponent = () => {
  return (
    <div>
      <BreadCrumbs />
      {/* --------------------------------------------------------------------------------*/}
      {/* Row*/}
      {/* --------------------------------------------------------------------------------*/}
      <ComponentCard title="Tooltip">
        <>
          {[
            {
              placement: 'top',
              text: 'Top',
            },
            {
              placement: 'bottom',
              text: 'Bottom',
            },
            {
              placement: 'left',
              text: 'Left',
            },
            {
              placement: 'right',
              text: 'Right',
            },
          ].map((tooltip, i) => {
            return <TooltipItem key={tooltip.placement} item={tooltip} id={i} />;
          })}
        </>
      </ComponentCard>
      {/* -------------------------------------------------------------------------------- */}
      {/* Row */}
      {/* -------------------------------------------------------------------------------- */}
    </div>
  );
};

export default TooltipComponent;
